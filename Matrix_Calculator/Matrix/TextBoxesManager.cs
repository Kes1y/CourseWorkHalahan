﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace Matrix
{
   public abstract class TextBoxesManager
   {
      public TextBox[,] TextBoxes;
      public int MaxLines
      {
         get;
         protected set;
      }
      public int MinLines
      {
         get;
         protected set;
      }
      public int PositionX
      {
         get;
         protected set;
      }
      public int PositionY
      {
         get;
         protected set;
      }
      protected Form Form;
      public Side Side;

      public TextBoxesManager(int x, int y, int minLines, int maxLines, Side side ,Form form)
      {
         Form = form;
         PositionX = x;
         PositionY = y;
         this.MaxLines = maxLines;
         this.MinLines = minLines;
         this.Side = side;
         TextBoxes = new TextBox[0, 0];
         ChangeTextBoxArray(minLines, minLines);
        }

      public void ChangeTextBoxArray(decimal x, decimal y)
      {
         TextBox[,] temps = new TextBox[(int)x, (int)y];

         bool formatPlusGrand = (temps.GetLength(0) > TextBoxes.GetLength(0) || temps.GetLength(1) > TextBoxes.GetLength(1));
         bool formatPlusPetit = temps.GetLength(0) < TextBoxes.GetLength(0) || temps.GetLength(1) < TextBoxes.GetLength(1);

         for (int i = 0; i < Math.Max(temps.GetLength(0), TextBoxes.GetLength(0)); ++i)
         {
            for (int j = 0; j < Math.Max(temps.GetLength(1), TextBoxes.GetLength(1)); ++j)
            {
               if (formatPlusGrand && (i >= TextBoxes.GetLength(0) || j >= TextBoxes.GetLength(1)))
               {
                   temps[i, j] = CreateOneTextBox(PositionX + 5 + j * 40, PositionY + 30 + i * 30, i+10*j+ ConvertSide(Side));
               }
               else if (formatPlusPetit && (i >= temps.GetLength(0) || j >= temps.GetLength(1)))
               {
                  TextBoxes[i, j].Dispose();
               }
               else
               {
                  temps[i, j] = TextBoxes[i, j];
               }
            }
         }
         TextBoxes = temps;
      }


      private TextBox CreateOneTextBox(int x, int y, int startingtabindex)
      {
         TextBox tb = new TextBox();
         tb.Location = new System.Drawing.Point(x, y);
         tb.Name = "TextBox_" + x + "_" + y;
         tb.Size = new System.Drawing.Size(30, 20);
         tb.TabIndex = startingtabindex;
         tb.TextChanged += new System.EventHandler(textBox_TextChanged);
         tb.Text = "0";
         Form.Controls.Add(tb);
         return tb;

      }

      private void textBox_TextChanged(object sender, EventArgs e)
      {
          TextBox tb = (sender as TextBox);
          float f;
          if (float.TryParse(tb.Text, out f))
          {
              tb.ForeColor = System.Drawing.Color.Black;
          }
          else
          {
              tb.ForeColor = System.Drawing.Color.Red;
          }
          if (tb.Text == "")
          {
             (Form as Form1).DisablerBouton();
          }
          else
          {
             (Form as Form1).VerifierBouton();
          }
          
      }

      protected NumericUpDown CreateNumericUpDown(int x, int y, EventHandler methode)
      {
         NumericUpDown numericUpDown1 = new System.Windows.Forms.NumericUpDown();
         numericUpDown1.Location = new System.Drawing.Point(x, y);
         numericUpDown1.Maximum = new decimal(new int[] { MaxLines, 0, 0, 0 });
         numericUpDown1.Minimum = new decimal(new int[] { MinLines, 0, 0, 0 });
         numericUpDown1.Name = "numericUpDown_" + x + "_" + y;
         numericUpDown1.Size = new System.Drawing.Size(30, 20);
         numericUpDown1.TabIndex = 1;
         numericUpDown1.Value = new decimal(new int[] { MinLines, 0, 0, 0 });
         numericUpDown1.ValueChanged += methode;
         Form.Controls.Add(numericUpDown1);
         return numericUpDown1;
      }

      public Matrix ToMatrix()
      {
         float[,] tabl = new float[TextBoxes.GetLength(0), TextBoxes.GetLength(1)];
            for (int i = 0; i < TextBoxes.GetLength(0); ++i)
            {
               for (int j = 0; j < TextBoxes.GetLength(1); ++j)
               {
                  tabl[i, j] = float.Parse(TextBoxes[i, j].Text);
               }
            }
         return new Matrix(tabl);
      }

      protected Label CreateLabel(int x, int y, string texte)
      {
         Label label1 = new Label();
         label1.AutoSize = true;
         label1.Location = new System.Drawing.Point(x, y);
         label1.Name = "label_" + x + "_" + y;
         label1.Size = new System.Drawing.Size(335, 313);
         label1.TabIndex = 1;
         label1.Text = texte;
         Form.Controls.Add(label1);
         return label1;
      }

      public virtual void Empty()
      {
         foreach (TextBox t in TextBoxes)
         {
            t.Dispose();
         }
      }

      int ConvertSide(Side side)
      {
          return side != Side.RIGHT ? 200 : 300; 
      }
   }
}
