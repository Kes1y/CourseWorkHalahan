﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Matrix
{
   class ShowResponse
   {
      Label[,] labels;
      protected Form Form;
      int PositionX
      {
         get;
         set;
      }
      int PositionY
      {
         get;
         set;
      }

      public ShowResponse(Matrix A, int x, int y, Form form)
      {
         PositionX = x;
         PositionY = y;
         Form = form;
         labels = CreateTableLabels(A);
      }

      Label[,] CreateTableLabels(Matrix A)
      {
         Label[,] tabl = new Label[A.M, A.N];
         for (int i = 0; i < tabl.GetLength(0); ++i)
         {
            for (int j = 0; j < tabl.GetLength(1); ++j)
            {
               tabl[i, j] = CreateLabel(A[i, j].ToString(), PositionX + j*50, PositionY + i * 20);
            }
         }
            return tabl;
      }

      protected virtual Label CreateLabel(string s, int x, int y)
      {
         Label label1 = new Label();
         label1.AutoSize = true;
         label1.BorderStyle = System.Windows.Forms.BorderStyle.None;
         label1.Location = new System.Drawing.Point(x, y);
         label1.Name = "label1";
         label1.MinimumSize = new System.Drawing.Size(50, 20);
         label1.Size = new System.Drawing.Size(50, 20);
         label1.TabIndex = 3;
         label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         label1.Text = s;
         Form.Controls.Add(label1);
         
         return label1;
      }

      public void Empty()
      {
         foreach (Label l in labels)
         {
            l.Dispose();
         }
      }
    }
}
