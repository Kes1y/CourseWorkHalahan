﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Matrix
{
   class TextBoxesManagerSquare : TextBoxesManager
   {
      NumericUpDown LinesColumns;
      Label LabelLinesColumns;

      public TextBoxesManagerSquare(int x, int y, int minLinges, int maxLignes, Side side, Form form)
         : base(x, y, minLinges, maxLignes, side, form)
      {
         base.ChangeTextBoxArray(minLinges, minLinges);
         LinesColumns = CreateNumericUpDown(PositionX + 100, PositionY + 5, new System.EventHandler(this.numericUpDown1_ValueChanged));
         LabelLinesColumns = CreateLabel(PositionX + 5, PositionY + 7, "Рядки/Стоіпці :");
      }

      public TextBoxesManagerSquare(Matrix matrix, int x, int y, int minLignes, int maxLignes, Side side, Form form)
         : base(x, y,minLignes, maxLignes, side, form)
      {

         LinesColumns = CreateNumericUpDown(PositionX + 100, PositionY + 5, new System.EventHandler(this.numericUpDown1_ValueChanged));
         LabelLinesColumns = CreateLabel(PositionX + 5, PositionY + 7, "Рядки/Стоіпці :");



         int m = matrix.M;
         int n = matrix.N;

         if (m > MaxLines)
         {
            m = MaxLines;
         }
         if (m < MinLines)
         {
            m = MinLines;
         }
         if (n > MaxLines)
         {
            n = MaxLines;
         }
         if (n < MinLines)
         {
            n = MinLines;
         }
         int max = Math.Max(m,n);
         LinesColumns.Value = max;
         ChangeTextBoxArray(max, max);
         for (int i = 0; i < Math.Min(matrix.M, m); ++i)
         {
            for (int j = 0; j < Math.Min( matrix.N,n); ++j)
            {
               TextBoxes[i, j].Text = matrix[i, j].ToString() ;
            }
         }

      }

      public TextBoxesManagerSquare(string[,] matrix, int x, int y, int minlignes, int maxLignes, Side side, Form form)
         : base(x, y, minlignes, maxLignes, side, form)
      {
         LinesColumns = CreateNumericUpDown(PositionX + 100, PositionY + 5, new System.EventHandler(this.numericUpDown1_ValueChanged));
         LabelLinesColumns = CreateLabel(PositionX + 5, PositionY + 7, "Рядки/Стоіпці :");



         int m = matrix.GetLength(0);
         int n = matrix.GetLength(1);

         if (m > MaxLines)
         {
            m = MaxLines;
         }
         if (m < MinLines)
         {
            m = MinLines;
         }
         if (n > MaxLines)
         {
            n = MaxLines;
         }
         if (n < MinLines)
         {
            n = MinLines;
         }
         int max = Math.Max(m, n);
         LinesColumns.Value = max;
         ChangeTextBoxArray(max, max);

         for (int i = 0; i < Math.Min(matrix.GetLength(0),m); ++i)
         {
            for (int j = 0; j < Math.Min(matrix.GetLength(1),n); ++j)
            {
               TextBoxes[i, j].Text = matrix[i, j].ToString();
            }
         }

         (Form as Form1).VerifierBouton();
      }

      private void numericUpDown1_ValueChanged(object sender, EventArgs e)
      {
         this.ChangeTextBoxArray(TextBoxes.GetLength(0), (sender as NumericUpDown).Value);
         this.ChangeTextBoxArray((sender as NumericUpDown).Value, TextBoxes.GetLength(1));
         (Form as Form1).VerifierBouton();
      }

      public override void Empty()
      {
         base.Empty();
         LinesColumns.Dispose();
         LabelLinesColumns.Dispose();
      }
   }
}
