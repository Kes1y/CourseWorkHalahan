﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Matrix
{
   public enum Side { RIGHT, LEFT }
   public partial class Form1 : Form
   {
      public TextBoxesManager RightS;
      public TextBoxesManager LeftS;
      ShowResponse Answer;
      string operation = "";

      public Form1()
      {
         InitializeComponent();
         VerifierBouton();
         this.Text = "Матричний калькулятор";
      }

      private void button1_Click(object sender, EventArgs e)
      {
         if (Answer != null)
         {
            Answer.Empty();
            Answer = null;
         }
         Matrix result = MakeOperation();
            Answer = new ShowResponse(result, (int)(Width / 2 - (50 * result.N / 2f)), 320, this);
      }

      private Matrix MakeOperation()
      {
         Matrix matrix = Matrix.Null(0, 0);
         switch (operation)
         {
            case "Додавання":
               matrix = LeftS.ToMatrix() + RightS.ToMatrix();
               break;
            case "Віднімання":
               matrix = LeftS.ToMatrix() - RightS.ToMatrix();
               break;
            case "Множення матриць":
               matrix = LeftS.ToMatrix() * RightS.ToMatrix();
               break;
            case "Транспонування":
               matrix = LeftS.ToMatrix().Transposition;
               break;
            case "Множення на число":
                matrix = LeftS.ToMatrix() * (RightS as TextBoxesManagerNumber).ToFloat();
                break;
            case "Визначник":
               float[,] tmp = new float[1, 1];
               tmp[0, 0] = LeftS.ToMatrix().Determinant;
               matrix = new Matrix(tmp);
               break;
            case "Зворотна матриця":
               matrix = LeftS.ToMatrix().Inverse;
               break;
         }

         return matrix;
      }

      private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
      {
         if (operation != (sender as ComboBox).Text)
         {
            operation = ((sender as ComboBox).Text);
            ChangerOperation();
            VerifierBouton();
         }
      }

      private void ChangerOperation()
      {
         int minLines = 1, maxLines = 7;

         if (LeftS == null)
         {
            LeftS = CreaterManager(LeftS, ChooseTextBoxesManagerMatrices(operation, Side.LEFT), minLines, maxLines, Side.LEFT);
         }
         else
         {
            LeftS = ChangerManager(LeftS, ChooseTextBoxesManagerMatrices(operation, Side.LEFT), minLines, maxLines);
         }
         if (RightS == null)
         {
            RightS = CreaterManager(RightS, ChooseTextBoxesManagerMatrices(operation, Side.RIGHT), minLines, maxLines, Side.RIGHT);
         }
         else
         {
            RightS = ChangerManager(RightS, ChooseTextBoxesManagerMatrices(operation, Side.RIGHT), minLines, maxLines);
         }
      }

      private TextBoxesManager ChangerManager(TextBoxesManager tbm, string newType, int minLignes, int maxLignes)
      {
         TextBoxesManager temps = null;
         switch (newType)
         {
            case "TextBoxesManagerRectangle":
               temps = new TextBoxesManagerRectangle(tbm.ToMatrix(), tbm.PositionX, tbm.PositionY, minLignes, maxLignes, tbm.Side, this);
               tbm.Empty();
               tbm = temps;
               break;
            case "TextBoxesManagerSquare":
                temps = new TextBoxesManagerSquare(tbm.ToMatrix(), tbm.PositionX, tbm.PositionY, minLignes, maxLignes, tbm.Side, this);
                tbm.Empty();
                tbm = temps;
                break;
            case "TextBoxesManagerNumber":
                temps = new TextBoxesManagerNumber(tbm.ToMatrix()[0, 0], tbm.PositionX, tbm.PositionY, tbm.Side, this);
                tbm.Empty();
                tbm = temps;
                break;
            case "null":
               tbm.Empty();
               tbm = null;
               temps = null;
               break;
         }
         return temps;
      }

      private TextBoxesManager CreaterManager(TextBoxesManager tbm, string newType, int minLignes, int maxLignes, Side side)
      {
         int x = side == Side.LEFT ? 0 : 520;
         int y = 20;
         TextBoxesManager temps = null;
         switch (newType)
         {
            case "TextBoxesManagerRectangle":
               temps = new TextBoxesManagerRectangle(x, y, minLignes, maxLignes, side, this);
               tbm = temps;
               break;
            case "TextBoxesManagerSquare":
                temps = new TextBoxesManagerSquare(x, y, minLignes, maxLignes, side, this);
                tbm = temps;
                break;
            case "TextBoxesManagerNumber":
                    temps = new TextBoxesManagerNumber(x, y, side, this);
                    tbm = temps;
               break;
         }
         return temps;
      }

      string ChooseTextBoxesManagerMatrices(string operation, Side side)
      {
         string s = "";
         if (operation == "Визначник" || operation == "Зворотна матриця" || operation == "Транспонування")
         {
            s = side == Side.LEFT ? "TextBoxesManagerSquare" : "null";
         }
         else if (operation == "Множення на число" && side == Side.RIGHT)
         {
            s = "TextBoxesManagerNumber";
         }
         else
         {
            s = "TextBoxesManagerRectangle";
         }
         return s;
      }

      public void VerifierBouton()
      {
         VerifierBoutonMatrices();

         if (operation == "")
         {
            DisablerBouton();
         }

      }

      void VerifierBoutonMatrices()
      {
            //if (MatricesInvalides() || !EstOperationValide())
            //if (Droite == null || MatricesInvalides() || Gauche.TextBoxes.GetLength(0) != Droite.TextBoxes.GetLength(0))
            //{
            //   btn_calculate.Enabled = false;
            //}
            //else
            //{
            btn_calculate.Enabled = true;
         //}
      }

      public void DisablerBouton()
      {
         btn_calculate.Enabled = false;
      }

      private void quitterToolStripMenuItem1_Click(object sender, EventArgs e)
      {
         this.Close();
      }
   }
}
