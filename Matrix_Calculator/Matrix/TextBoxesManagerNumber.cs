﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Matrix
{
   class TextBoxesManagerNumber : TextBoxesManager
   {
      Label Label;

      public TextBoxesManagerNumber(int x, int y, Side side,Form form)
         : base(x, y,1, 1, side, form)
      {
         Label = CreateLabel(PositionX + 5, PositionY + 7, "Число :");
      }

      public TextBoxesManagerNumber(float f, int x, int y, Side side, Form form)
         : base(x, y, 1,1, side, form)
      {
         Label = CreateLabel(PositionX + 5, PositionY + 7, "Число :");
         TextBoxes[0, 0].Text = f.ToString();
      }
      public TextBoxesManagerNumber(string s, int x, int y, Side side, Form form)
         : base(x, y,1, 1, side, form)
      {
         Label = CreateLabel(PositionX + 5, PositionY + 7, "Число :");
         TextBoxes[0, 0].Text = s;
         (Form as Form1).VerifierBouton();
      }

      public override void Empty()
      {
         base.Empty();
         Label.Dispose();
      }

      public float ToFloat()
      {
         return float.Parse(TextBoxes[0, 0].Text);
      }
   }
}
