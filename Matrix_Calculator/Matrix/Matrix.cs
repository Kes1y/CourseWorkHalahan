﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Matrix
{
    public class Matrix
    {
        float[,] Elements { get; set; }

        public Matrix(float[,] elements)
        {
            Elements = elements;
        }

        public int M
        {
            get
            {
                return Elements.GetLength(0);
            }
        }

        public int N
        {
            get
            {
                return Elements.GetLength(1);
            }
        }

        public float this[int m, int n]
        {
            get
            {
                return Elements[m, n];
            }
        }

        static Matrix Addition(Matrix A, Matrix B)
        {
            float[,] elements = new float[0, 0];
            if (A.M == B.M && A.N == B.N)
            {
                elements = new float[A.M, A.N];
                for (int i = 0; i < A.M; ++i)
                {
                    for (int j = 0; j < A.N; ++j)
                    {
                        elements[i, j] = A[i, j] + B[i, j];
                    }
                }
            }

            return new Matrix(elements);
        }

        static Matrix Multiplication(Matrix A, Matrix B)
        {
            float[,] elements = new float[0, 0];
            if (A.N == B.M)
            {
                elements = new float[A.M, B.N];
                for (int i = 0; i < elements.GetLength(0); ++i)
                {
                    for (int j = 0; j < elements.GetLength(1); ++j)
                    {
                        float sum = 0;
                        for (int k = 0; k < A.N; ++k)
                        {
                            sum += A[i, k] * B[k, j];
                        }
                        elements[i, j] = sum;
                    }
                }
            }

            return new Matrix(elements);
        }

        static Matrix MultiplicationNumber(Matrix A, float f)
        {
            float[,] elements = new float[A.M, A.N];

            for (int i = 0; i < elements.GetLength(0); ++i)
            {
                for (int j = 0; j < elements.GetLength(1); ++j)
                {
                    elements[i, j] = A[i, j] * f;
                }
            }

            return new Matrix(elements);
        }

        static Matrix MakeTransposition(Matrix A)
        {
            float[,] elements = new float[A.N, A.M];
            for (int i = 0; i < A.M; ++i)
            {
                for (int j = 0; j < A.N; ++j)
                {
                    elements[j, i] = A[i, j];
                }
            }


            return new Matrix(elements);
        }

        static float FindDeterminant(Matrix A)
        {
            float res = 0;

            if (A.M == 2)
            {
                res = A[0, 0] * A[1, 1] - A[0, 1] * A[1, 0];
            }
            else if (A.M == 1)
            {
                res = A[0, 0];
            }
            else
            {
                for (int i = 0; i < A.M; ++i)
                {
                    float number = A[0, i];
                    if (i % 2 == 1)
                    {
                        number *= -1;
                    }
                    res += number * FindDeterminant(A.Minor(0, i));
                }
            }
            return res;
        }

        public Matrix Minor(int m, int n)
        {
            float[,] elements = new float[M - 1, N - 1];
            for (int i = 0; i < M; ++i)
            {
                for (int j = 0; j < N; ++j)
                {
                    if (i != m && j != n)
                    {
                        int positioni = i;
                        int positionj = j;
                        if (i > m)
                        {
                            --positioni;
                        }
                        if (j > n)
                        {
                            --positionj;
                        }

                        elements[positioni, positionj] = this[i, j];
                    }
                }
            }
            return new Matrix(elements);
        }

        public Matrix Co_factors
        {
            get
            {
                float[,] elements = new float[this.M, this.N];
                for (int i = 0; i < elements.GetLength(0); ++i)
                {
                    for (int j = 0; j < elements.GetLength(1); ++j)
                    {
                        float multiplimode = 1;
                        if ((i + j) % 2 == 1)
                        {
                            multiplimode = -1;
                        }
                        elements[i, j] = multiplimode * (this.Minor(i, j).Determinant);
                    }
                }
                return new Matrix(elements);

            }
        }

        public float Determinant
        {
            get
            {
                return FindDeterminant(this);
            }
        }

        public Matrix Transposition
        {
            get
            {
                return MakeTransposition(this);
            }
        }

        public Matrix Inverse
        {
            get
            {
                float res = this.Determinant;

                return (1f / res) * this.Co_factors.Transposition;
            }
        }

        public static Matrix Null(int m, int n)
        {
            float[,] tabl = new float[m, n];
            for (int i = 0; i < m; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    tabl[i, j] = 0;
                }
            }
            return new Matrix(tabl);
        }

        public static Matrix operator +(Matrix A, Matrix B)
        {
            return Addition(A, B);
        }

        public static Matrix operator -(Matrix A, Matrix B)
        {
            return Addition(A, -1 * B);
        }

        public static Matrix operator *(Matrix A, Matrix B)
        {
            return Multiplication(A, B);
        }

        public static Matrix operator *(Matrix A, float f)
        {
            return MultiplicationNumber(A, f);
        }

        public static Matrix operator *(float f, Matrix A)
        {
            return MultiplicationNumber(A, f);
        }
    }
}
