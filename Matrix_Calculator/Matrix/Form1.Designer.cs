﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Matrix
{
   partial class Form1
   {
      private System.ComponentModel.IContainer components = null;

      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      private void InitializeComponent()
      {
            this.btn_calculate = new System.Windows.Forms.Button();
            this.cb_operation = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();


            this.btn_calculate.Location = new System.Drawing.Point(353, 213);
            this.btn_calculate.Name = "btn_calculate";
            this.btn_calculate.Size = new System.Drawing.Size(98, 23);
            this.btn_calculate.TabIndex = 22;
            this.btn_calculate.Text = "Розрахувати";
            this.btn_calculate.Click += new System.EventHandler(this.button1_Click);


            this.cb_operation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_operation.Items.AddRange(new object[] {
            "Додавання",
            "Віднімання",
            "Множення матриць",
            "Транспонування",
            "Множення на число",
            "Визначник",
            "Зворотна матриця"});
            this.cb_operation.Location = new System.Drawing.Point(343, 186);
            this.cb_operation.Name = "cb_operation";
            this.cb_operation.Size = new System.Drawing.Size(121, 21);
            this.cb_operation.TabIndex = 21;
            this.cb_operation.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
 

            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(375, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Операція";


            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cb_operation);
            this.Controls.Add(this.btn_calculate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Матричний калькулятор";
            this.ResumeLayout(false);
            this.PerformLayout();

      }

      private System.Windows.Forms.Button btn_calculate;
      private ComboBox cb_operation;
      private Label label2;
      

   }
}

